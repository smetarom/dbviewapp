package smetana.dbview;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import smetana.dbview.controller.DBConnectionController;
import smetana.dbview.dto.DBConnectionDetailDto;
import smetana.dbview.entity.DBConnectionDetail;
import smetana.dbview.repository.DBConnectionDetailRepository;
import smetana.dbview.service.PasswordService;

import java.util.Optional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DBConnectionIT {

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private DBConnectionController dbConnectionController;
    @Autowired
    private PasswordService passwordService;
    @Autowired
    private DBConnectionDetailRepository dbConnectionDetailRepository;

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.dbConnectionController).build();
    }

    @Test
    public void listConnections() throws Exception {
        DBConnectionDetail savedDbConnectionDetail = dbConnectionDetailRepository.save(new DBConnectionDetail("test" + System.currentTimeMillis(), "hostname", 3306, "databaseName", "username", "password"));
        long countRecords = dbConnectionDetailRepository.count();

        mockMvc.perform(get("/connection/list").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(Long.valueOf(countRecords).intValue())));
    }

    @Test
    public void getConnectionById() throws Exception {
        DBConnectionDetail savedDbConnectionDetail = dbConnectionDetailRepository.save(new DBConnectionDetail("test" + System.currentTimeMillis(), "hostname", 3306, "databaseName", "username", "password"));

        mockMvc.perform(get("/connection/" + savedDbConnectionDetail.getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", Matchers.equalTo(savedDbConnectionDetail.getName())));
    }

    @Test
    public void createConnection() throws Exception {
        DBConnectionDetailDto dbConnectionDetailDto = new DBConnectionDetailDto("test" + System.currentTimeMillis(), "hostname", 3306, "databaseName", "username", "password");
        String requestJson = objectToJSON(dbConnectionDetailDto);

        mockMvc.perform(post("/connection").contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isOk());

        Optional<DBConnectionDetail> persistedConnectionOpt = dbConnectionDetailRepository.findByName(dbConnectionDetailDto.getName());
        assert persistedConnectionOpt.isPresent();
        DBConnectionDetail persistedConnection = persistedConnectionOpt.get();
        assert persistedConnection.getName().equals(dbConnectionDetailDto.getName());
        assert persistedConnection.getHostname().equals(dbConnectionDetailDto.getHostname());
        assert persistedConnection.getPort().equals(dbConnectionDetailDto.getPort());
        assert persistedConnection.getDatabaseName().equals(dbConnectionDetailDto.getDatabaseName());
        assert persistedConnection.getUsername().equals(dbConnectionDetailDto.getUsername());
        assert passwordService.decryptPassword(persistedConnection.getPassword()).equals(dbConnectionDetailDto.getPassword());
    }

    @Test
    public void createConnectionWithDuplicatedName() throws Exception {
        DBConnectionDetail savedDbConnectionDetail = dbConnectionDetailRepository.save(new DBConnectionDetail("test" + System.currentTimeMillis(), "hostname", 3306, "databaseName", "username", "password"));

        DBConnectionDetailDto dbConnectionDetailDto = new DBConnectionDetailDto(savedDbConnectionDetail.getName(), "hostname", 3306, "databaseName", "username", "password");
        String requestJson = objectToJSON(dbConnectionDetailDto);

        mockMvc.perform(post("/connection").contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateConnection() throws Exception {
        DBConnectionDetail savedDbConnectionDetail = dbConnectionDetailRepository.save(new DBConnectionDetail("test" + System.currentTimeMillis(), "hostname", 3306, "databaseName", "username", "password"));

        DBConnectionDetailDto dbConnectionDetailDto = new DBConnectionDetailDto("test" + System.currentTimeMillis(), "hostname2", 3307, "databaseName2", "username2", "password2");
        String requestJson = objectToJSON(dbConnectionDetailDto);

        mockMvc.perform(put("/connection/" + savedDbConnectionDetail.getId()).contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isOk());

        Optional<DBConnectionDetail> persistedConnectionOpt = dbConnectionDetailRepository.findByName(dbConnectionDetailDto.getName());
        assert persistedConnectionOpt.isPresent();
        DBConnectionDetail persistedConnection = persistedConnectionOpt.get();
        assert persistedConnection.getName().equals(dbConnectionDetailDto.getName());
        assert persistedConnection.getHostname().equals(dbConnectionDetailDto.getHostname());
        assert persistedConnection.getPort().equals(dbConnectionDetailDto.getPort());
        assert persistedConnection.getDatabaseName().equals(dbConnectionDetailDto.getDatabaseName());
        assert persistedConnection.getUsername().equals(dbConnectionDetailDto.getUsername());
        assert passwordService.decryptPassword(persistedConnection.getPassword()).equals(dbConnectionDetailDto.getPassword());
    }

    @Test
    public void updateConnectionWithDuplicatedName() throws Exception {
        DBConnectionDetail savedDbConnectionDetail = dbConnectionDetailRepository.save(new DBConnectionDetail("test" + System.currentTimeMillis(), "hostname", 3306, "databaseName", "username", "password"));
        DBConnectionDetail savedDbConnectionDetail2 = dbConnectionDetailRepository.save(new DBConnectionDetail("test" + System.currentTimeMillis(), "hostname", 3306, "databaseName", "username", "password"));

        DBConnectionDetailDto dbConnectionDetailDto = new DBConnectionDetailDto(savedDbConnectionDetail2.getName(), "hostname2", 3307, "databaseName2", "username2", "password2");
        String requestJson = objectToJSON(dbConnectionDetailDto);

        mockMvc.perform(put("/connection/" + savedDbConnectionDetail.getId()).contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void updateNonExistingConnection() throws Exception {
        DBConnectionDetailDto dbConnectionDetailDto = new DBConnectionDetailDto("test" + System.currentTimeMillis(), "hostname2", 3307, "databaseName2", "username2", "password2");
        String requestJson = objectToJSON(dbConnectionDetailDto);

        mockMvc.perform(put("/connection/" + Integer.MIN_VALUE).contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteConnection() throws Exception {
        DBConnectionDetail savedDbConnectionDetail = dbConnectionDetailRepository.save(new DBConnectionDetail("test" + System.currentTimeMillis(), "hostname", 3306, "databaseName", "username", "password"));

        mockMvc.perform(delete("/connection/" + savedDbConnectionDetail.getId()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Optional<DBConnectionDetail> persistedConnectionOpt = dbConnectionDetailRepository.findByName(savedDbConnectionDetail.getName());
        assert !persistedConnectionOpt.isPresent();
    }

    @Test
    public void deleteNonExistingConnection() throws Exception {
        mockMvc.perform(delete("/connection/" + Integer.MIN_VALUE).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    private String objectToJSON(Object object) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(object);
    }


}
