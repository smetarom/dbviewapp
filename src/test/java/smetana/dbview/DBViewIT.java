package smetana.dbview;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import smetana.dbview.controller.DBViewController;
import smetana.dbview.entity.DBConnectionDetail;
import smetana.dbview.repository.DBConnectionDetailRepository;
import smetana.dbview.service.PasswordService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import java.text.SimpleDateFormat;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DBViewIT {

    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private DBViewController dbViewController;
    @Autowired
    private PasswordService passwordService;
    @Autowired
    private DBConnectionDetailRepository dbConnectionDetailRepository;
    @Autowired
    private EntityManagerFactory entityManagerFactory;

    private String tableName;
    private String connectionName;

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.dbViewController).build();

        EntityTransaction transaction = null;
        EntityManager entityManager = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            transaction = entityManager.getTransaction();
            transaction.begin();

            tableName = "TEST_" + System.currentTimeMillis();
            String createTableQuery = "CREATE TABLE " + tableName + " (\n" +
                    "number_field INT(6) AUTO_INCREMENT PRIMARY KEY,\n" +
                    "text_field VARCHAR(30) UNIQUE,\n" +
                    "date_field DATE,\n" +
                    "boolean_field BOOLEAN NOT NULL default 0\n" +
                    ")";
            entityManager.createNativeQuery(createTableQuery).executeUpdate();

            DBConnectionDetail savedDbConnectionDetail = dbConnectionDetailRepository.save(new DBConnectionDetail("test" + System.currentTimeMillis(), "localhost", 3306, "dbview_test", "dbview", passwordService.encryptPassword("password")));
            connectionName = savedDbConnectionDetail.getName();

            transaction.commit();
        } catch (Throwable e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            throw e;
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    @Test
    public void listTables() throws Exception {
        mockMvc.perform(get("/dbview/" + connectionName + "/tables"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasItem(tableName)));
    }

    @Test
    public void listTableColumns() throws Exception {
        mockMvc.perform(get("/dbview/" + connectionName + "/table/" + tableName + "/columns"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(4)))
                .andExpect(jsonPath("$[0].field", Matchers.equalTo("number_field")))
                .andExpect(jsonPath("$[0].type", Matchers.equalTo("int(6)")))
                .andExpect(jsonPath("$[0].nullable", Matchers.equalTo(false)))
                .andExpect(jsonPath("$[0].key", Matchers.equalTo("PRI")))
                .andExpect(jsonPath("$[0].defaultValue", Matchers.nullValue()))
                .andExpect(jsonPath("$[0].extra", Matchers.equalTo("auto_increment")))
                .andExpect(jsonPath("$[1].field", Matchers.equalTo("text_field")))
                .andExpect(jsonPath("$[1].type", Matchers.equalTo("varchar(30)")))
                .andExpect(jsonPath("$[1].nullable", Matchers.equalTo(true)))
                .andExpect(jsonPath("$[1].key", Matchers.equalTo("UNI")))
                .andExpect(jsonPath("$[1].defaultValue", Matchers.nullValue()))
                .andExpect(jsonPath("$[1].extra", Matchers.equalTo("")))
                .andExpect(jsonPath("$[2].field", Matchers.equalTo("date_field")))
                .andExpect(jsonPath("$[2].type", Matchers.equalTo("date")))
                .andExpect(jsonPath("$[2].nullable", Matchers.equalTo(true)))
                .andExpect(jsonPath("$[2].key", Matchers.equalTo("")))
                .andExpect(jsonPath("$[2].defaultValue", Matchers.nullValue()))
                .andExpect(jsonPath("$[2].extra", Matchers.equalTo("")))
                .andExpect(jsonPath("$[3].field", Matchers.equalTo("boolean_field")))
                .andExpect(jsonPath("$[3].type", Matchers.equalTo("tinyint(1)")))
                .andExpect(jsonPath("$[3].nullable", Matchers.equalTo(false)))
                .andExpect(jsonPath("$[3].key", Matchers.equalTo("")))
                .andExpect(jsonPath("$[3].defaultValue", Matchers.equalTo("0")))
                .andExpect(jsonPath("$[3].extra", Matchers.equalTo("")))
        ;
    }

    @Test
    public void getTableData() throws Exception {
        EntityTransaction transaction = null;
        EntityManager entityManager = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            transaction = entityManager.getTransaction();
            transaction.begin();

            String insertQuery = "INSERT INTO " + tableName + " (number_field, text_field, date_field, boolean_field) VALUES (NULL, 'text', CURDATE(), 1)";
            entityManager.createNativeQuery(insertQuery).executeUpdate();

            transaction.commit();
        } catch (Throwable e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            throw e;
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        mockMvc.perform(get("/dbview/" + connectionName + "/table/" + tableName + "/data"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].number_field", Matchers.equalTo(1)))
                .andExpect(jsonPath("$[0].text_field", Matchers.equalTo("text")))
                .andExpect(jsonPath("$[0].boolean_field", Matchers.equalTo(true)))
                ;
    }

    @Test
    public void getTableStatistics() throws Exception {
        EntityTransaction transaction = null;
        EntityManager entityManager = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            transaction = entityManager.getTransaction();
            transaction.begin();

            String insertQuery = "INSERT INTO " + tableName + " (number_field, text_field, date_field, boolean_field) VALUES (NULL, 'text', CURDATE(), 1)";
            entityManager.createNativeQuery(insertQuery).executeUpdate();
            insertQuery = "INSERT INTO " + tableName + " (number_field, text_field, date_field, boolean_field) VALUES (NULL, 'text1', CURDATE() - 1, 0)";
            entityManager.createNativeQuery(insertQuery).executeUpdate();
            insertQuery = "INSERT INTO " + tableName + " (number_field, text_field, date_field, boolean_field) VALUES (NULL, 'text2', CURDATE() + 1, 1)";
            entityManager.createNativeQuery(insertQuery).executeUpdate();

            transaction.commit();
        } catch (Throwable e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            throw e;
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }

        mockMvc.perform(get("/dbview/" + connectionName + "/table/" + tableName + "/statistics"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numberOfAttributes", Matchers.equalTo(4)))
                .andExpect(jsonPath("$.numberOfRecords", Matchers.equalTo(3)))
        ;
    }

    @Test
    public void tableColumnStatisticsEmptyTable() throws Exception {

        mockMvc.perform(get("/dbview/" + connectionName + "/table/" + tableName + "/column/number_field/statistics"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.min", Matchers.nullValue()))
                .andExpect(jsonPath("$.max", Matchers.nullValue()))
                .andExpect(jsonPath("$.avg", Matchers.nullValue()))
                .andExpect(jsonPath("$.median", Matchers.nullValue()))
        ;

        mockMvc.perform(get("/dbview/" + connectionName + "/table/" + tableName + "/column/text_field/statistics"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.min", Matchers.nullValue()))
                .andExpect(jsonPath("$.max", Matchers.nullValue()))
                .andExpect(jsonPath("$.avg", Matchers.nullValue()))
                .andExpect(jsonPath("$.median", Matchers.nullValue()))
        ;

        mockMvc.perform(get("/dbview/" + connectionName + "/table/" + tableName + "/column/date_field/statistics"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.min", Matchers.nullValue()))
                .andExpect(jsonPath("$.max", Matchers.nullValue()))
                .andExpect(jsonPath("$.avg", Matchers.nullValue()))
                .andExpect(jsonPath("$.median", Matchers.nullValue()))
        ;

        mockMvc.perform(get("/dbview/" + connectionName + "/table/" + tableName + "/column/boolean_field/statistics"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.min", Matchers.nullValue()))
                .andExpect(jsonPath("$.max", Matchers.nullValue()))
                .andExpect(jsonPath("$.avg", Matchers.nullValue()))
                .andExpect(jsonPath("$.median", Matchers.nullValue()))
        ;
    }


    @Test
    public void tableColumnStatistics() throws Exception {
        EntityTransaction transaction = null;
        EntityManager entityManager = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            transaction = entityManager.getTransaction();
            transaction.begin();

            String insertQuery = "INSERT INTO " + tableName + " (number_field, text_field, date_field, boolean_field) VALUES (NULL, 'text', CURDATE(), 1)";
            entityManager.createNativeQuery(insertQuery).executeUpdate();
            insertQuery = "INSERT INTO " + tableName + " (number_field, text_field, date_field, boolean_field) VALUES (NULL, 'text1', CURDATE() - 1, 0)";
            entityManager.createNativeQuery(insertQuery).executeUpdate();
            insertQuery = "INSERT INTO " + tableName + " (number_field, text_field, date_field, boolean_field) VALUES (NULL, 'text2', CURDATE() + 3, 1)";
            entityManager.createNativeQuery(insertQuery).executeUpdate();

            transaction.commit();
        } catch (Throwable e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            throw e;
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }

        mockMvc.perform(get("/dbview/" + connectionName + "/table/" + tableName + "/column/number_field/statistics"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.min", Matchers.equalTo(1)))
                .andExpect(jsonPath("$.max", Matchers.equalTo(3)))
                .andExpect(jsonPath("$.avg", Matchers.equalTo(2.0)))
                .andExpect(jsonPath("$.median", Matchers.equalTo(2)))
        ;

        mockMvc.perform(get("/dbview/" + connectionName + "/table/" + tableName + "/column/text_field/statistics"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.min", Matchers.equalTo("text")))
                .andExpect(jsonPath("$.max", Matchers.equalTo("text2")))
                .andExpect(jsonPath("$.avg", Matchers.nullValue()))
                .andExpect(jsonPath("$.median", Matchers.equalTo("text1")))
        ;

        mockMvc.perform(get("/dbview/" + connectionName + "/table/" + tableName + "/column/date_field/statistics"))
                .andExpect(status().isOk())
                ;

        mockMvc.perform(get("/dbview/" + connectionName + "/table/" + tableName + "/column/boolean_field/statistics"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.min", Matchers.equalTo(false)))
                .andExpect(jsonPath("$.max", Matchers.equalTo(true)))
                .andExpect(jsonPath("$.avg", Matchers.equalTo(0.6667)))
                .andExpect(jsonPath("$.median", Matchers.equalTo(true)))
        ;
    }

    @Test
    public void tableColumnStatisticsEvenRecords() throws Exception {
        EntityTransaction transaction = null;
        EntityManager entityManager = null;
        try {
            entityManager = entityManagerFactory.createEntityManager();
            transaction = entityManager.getTransaction();
            transaction.begin();

            String insertQuery = "INSERT INTO " + tableName + " (number_field, text_field, date_field, boolean_field) VALUES (NULL, 'text', CURDATE(), 1)";
            entityManager.createNativeQuery(insertQuery).executeUpdate();
            insertQuery = "INSERT INTO " + tableName + " (number_field, text_field, date_field, boolean_field) VALUES (NULL, 'text1', CURDATE() - 1, 0)";
            entityManager.createNativeQuery(insertQuery).executeUpdate();
            insertQuery = "INSERT INTO " + tableName + " (number_field, text_field, date_field, boolean_field) VALUES (NULL, 'text2', CURDATE() + 3, 1)";
            entityManager.createNativeQuery(insertQuery).executeUpdate();
            insertQuery = "INSERT INTO " + tableName + " (number_field, text_field, date_field, boolean_field) VALUES (NULL, 'text3', CURDATE() + 2, 1)";
            entityManager.createNativeQuery(insertQuery).executeUpdate();

            transaction.commit();
        } catch (Throwable e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            throw e;
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }

        mockMvc.perform(get("/dbview/" + connectionName + "/table/" + tableName + "/column/number_field/statistics"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.min", Matchers.equalTo(1)))
                .andExpect(jsonPath("$.max", Matchers.equalTo(4)))
                .andExpect(jsonPath("$.avg", Matchers.equalTo(2.5)))
                .andExpect(jsonPath("$.median", Matchers.equalTo(2.5)))
        ;

        mockMvc.perform(get("/dbview/" + connectionName + "/table/" + tableName + "/column/text_field/statistics"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.min", Matchers.equalTo("text")))
                .andExpect(jsonPath("$.max", Matchers.equalTo("text3")))
                .andExpect(jsonPath("$.avg", Matchers.nullValue()))
                .andExpect(jsonPath("$.median", Matchers.equalTo("text1")))
        ;

        mockMvc.perform(get("/dbview/" + connectionName + "/table/" + tableName + "/column/date_field/statistics"))
                .andExpect(status().isOk())
        ;

        mockMvc.perform(get("/dbview/" + connectionName + "/table/" + tableName + "/column/boolean_field/statistics"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.min", Matchers.equalTo(false)))
                .andExpect(jsonPath("$.max", Matchers.equalTo(true)))
                .andExpect(jsonPath("$.avg", Matchers.equalTo(0.75)))
                .andExpect(jsonPath("$.median", Matchers.equalTo(true)))
        ;
    }


}
