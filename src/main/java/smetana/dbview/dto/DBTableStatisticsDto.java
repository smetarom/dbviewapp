package smetana.dbview.dto;

/**
 * Statistics for a DB table
 */
public class DBTableStatisticsDto {
    /**
     * Number of records in the table
     */
    private int numberOfRecords;
    /**
     * Number of attributes of the table
     */
    private int numberOfAttributes;

    public DBTableStatisticsDto(int numberOfRecords, int numberOfAttributes) {
        this.numberOfRecords = numberOfRecords;
        this.numberOfAttributes = numberOfAttributes;
    }

    public int getNumberOfRecords() {
        return numberOfRecords;
    }

    public int getNumberOfAttributes() {
        return numberOfAttributes;
    }
}
