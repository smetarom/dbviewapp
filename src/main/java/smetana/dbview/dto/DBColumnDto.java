package smetana.dbview.dto;

/**
 * Representation of DB table column
 */
public class DBColumnDto {
    /**
     * Column name
     */
    private String field;
    /**
     * Column type
     */
    private String type;
    /**
     * If column is nullable
     */
    private boolean nullable;
    /**
     * Key is defined on the column
     */
    private String key;
    /**
     * Default value defined for a column
     */
    private String defaultValue;
    /**
     * Extra column properties
     */
    private String extra;

    public DBColumnDto(String field, String type, boolean nullable, String key, String defaultValue, String extra) {
        this.field = field;
        this.type = type;
        this.nullable = nullable;
        this.key = key;
        this.defaultValue = defaultValue;
        this.extra = extra;
    }

    public String getField() {
        return field;
    }

    public String getType() {
        return type;
    }

    public boolean isNullable() {
        return nullable;
    }

    public String getKey() {
        return key;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public String getExtra() {
        return extra;
    }

    @Override
    public String toString() {
        return "DBColumnDto{" +
                "field='" + field + '\'' +
                ", type='" + type + '\'' +
                ", nullable=" + nullable +
                ", key='" + key + '\'' +
                ", defaultValue='" + defaultValue + '\'' +
                ", extra='" + extra + '\'' +
                '}';
    }
}
