package smetana.dbview.dto;

import javax.validation.constraints.NotNull;

/**
 * Representation of DB connection detail
 */
public class DBConnectionDetailDto {
    /**
     * DB connection identifier
     */
    @NotNull
    private String name;
    /**
     * DB hostname
     */
    @NotNull
    private String hostname;
    /**
     * DB port
     */
    @NotNull
    private Integer port;
    /**
     * DB name
     */
    @NotNull
    private String databaseName;
    /**
     * DB username
     */
    @NotNull
    private String username;
    /**
     * DB user password
     */
    @NotNull
    private String password;

    public DBConnectionDetailDto() {
    }

    public DBConnectionDetailDto(String name, String hostname, Integer port, String databaseName, String username, String password) {
        this.name = name;
        this.hostname = hostname;
        this.port = port;
        this.databaseName = databaseName;
        this.username = username;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "DBConnectionDetailDto{" +
                "name='" + name + '\'' +
                ", hostname='" + hostname + '\'' +
                ", port=" + port +
                ", databaseName='" + databaseName + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
