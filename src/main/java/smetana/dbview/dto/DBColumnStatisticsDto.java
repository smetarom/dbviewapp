package smetana.dbview.dto;

/**
 * Statistics for a DB table column
 */
public class DBColumnStatisticsDto {
    /**
     * Min value in the column
     */
    private Object min;
    /**
     * Max value in the column
     */
    private Object max;
    /**
     * Average value in the column
     */
    private Object avg;
    /**
     * Median of the column
     */
    private Object median;

    public DBColumnStatisticsDto(Object min, Object max, Object avg, Object median) {
        this.min = min;
        this.max = max;
        this.avg = avg;
        this.median = median;
    }

    public Object getMin() {
        return min;
    }

    public void setMin(Object min) {
        this.min = min;
    }

    public Object getMax() {
        return max;
    }

    public void setMax(Object max) {
        this.max = max;
    }

    public Object getAvg() {
        return avg;
    }

    public void setAvg(Object avg) {
        this.avg = avg;
    }

    public Object getMedian() {
        return median;
    }

    public void setMedian(Object median) {
        this.median = median;
    }

    @Override
    public String toString() {
        return "DBColumnStatisticsDto{" +
                "min=" + min +
                ", max=" + max +
                ", avg=" + avg +
                ", median=" + median +
                '}';
    }
}
