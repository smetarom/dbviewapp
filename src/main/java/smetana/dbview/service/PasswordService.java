package smetana.dbview.service;

import org.jasypt.util.text.BasicTextEncryptor;
import org.springframework.stereotype.Service;

/**
 * Password encrypt/decrypt service
 */
@Service
public class PasswordService {
    private BasicTextEncryptor passwordEncryptor;

    public PasswordService(BasicTextEncryptor passwordEncryptor) {
        this.passwordEncryptor = passwordEncryptor;
    }

    public String encryptPassword(String plainPassword) {
        return passwordEncryptor.encrypt(plainPassword);
    }

    public String decryptPassword(String encryptedPassword) {
        return passwordEncryptor.decrypt(encryptedPassword);
    }
}
