package smetana.dbview.service;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import smetana.dbview.dto.DBColumnDto;
import smetana.dbview.dto.DBColumnStatisticsDto;
import smetana.dbview.dto.DBTableStatisticsDto;
import smetana.dbview.entity.DBConnectionDetail;
import smetana.dbview.exception.DBConnectionDetailNotFoundException;
import smetana.dbview.repository.DBConnectionDetailRepository;
import smetana.dbview.service.mapper.DbColumnMapper;
import smetana.dbview.service.mapper.DbColumnStatisticsMapper;
import smetana.dbview.service.mapper.DbTableStatisticsMapper;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service for working with DB connection
 */
@Service
public class DBViewService {
    private static String MYSQL_DRIVER_NAME = "com.mysql.cj.jdbc.Driver";

    private DBConnectionDetailRepository dbConnectionDetailRepository;
    private PasswordService passwordService;

    public DBViewService(DBConnectionDetailRepository dbConnectionDetailRepository, PasswordService passwordService) {
        this.dbConnectionDetailRepository = dbConnectionDetailRepository;
        this.passwordService = passwordService;
    }

    /**
     * Gets all DB tables in the database
     *
     * @param connectionName connection name
     * @return DB table names
     */
    @Transactional
    public List<String> getTables(String connectionName) {
        HikariDataSource dataSource = null;
        try {
            dataSource = createDataSource(connectionName);
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            return jdbcTemplate.queryForList("SHOW TABLES", String.class);
        } finally {
            closeConnection(dataSource);
        }
    }

    /**
     * Gets all columns in the table
     *
     * @param connectionName connection name
     * @param table          table name
     * @return DB column details
     */
    @Transactional
    public List<DBColumnDto> getTableColumns(String connectionName, String table) {
        HikariDataSource dataSource = null;
        try {
            dataSource = createDataSource(connectionName);
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            return jdbcTemplate.query("DESCRIBE " + table, new DbColumnMapper());
        } finally {
            closeConnection(dataSource);
        }
    }

    /**
     * Gets table data
     *
     * @param connectionName connection name
     * @param tableName      table name
     * @return table content
     */
    @Transactional
    public List<Map<String, Object>> getTableData(String connectionName, String tableName) {
        HikariDataSource dataSource = null;
        try {
            dataSource = createDataSource(connectionName);
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            return jdbcTemplate.queryForList("SELECT * FROM " + tableName);
        } finally {
            closeConnection(dataSource);
        }
    }

    /**
     * Calculates DB table statistics - number of records and number of attributes
     *
     * @param connectionName connection name
     * @param tableName      table name
     * @return DB table statistics
     */
    @Transactional
    public DBTableStatisticsDto calculateTableStatistics(String connectionName, String tableName) {
        HikariDataSource dataSource = null;
        try {
            dataSource = createDataSource(connectionName);
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            String query = String.format("SELECT (SELECT COUNT(*) FROM %s) as RECORDS, (SELECT COUNT(*) FROM information_schema.columns WHERE table_name = '%s') AS ATTRIBUTES", tableName, tableName);
            return jdbcTemplate.queryForObject(query, new DbTableStatisticsMapper());
        } finally {
            closeConnection(dataSource);
        }
    }

    /**
     * Calculates DB column statistics - min, max, average, median
     *
     * @param connectionName connection name
     * @param tableName      table name
     * @param columnName     column name
     * @return DB column statistics
     */
    @Transactional
    public DBColumnStatisticsDto calculateColumnStatistics(String connectionName, String tableName, String columnName) {
        HikariDataSource dataSource = null;
        try {
            dataSource = createDataSource(connectionName);
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
            String query = String.format("SELECT MIN(%s) as MIN, MAX(%s) as MAX, AVG(%s) as AVG FROM %s", columnName, columnName, columnName, tableName);
            DBColumnStatisticsDto dbColumnStatisticsDto = jdbcTemplate.queryForObject(query, new DbColumnStatisticsMapper());
            if (dbColumnStatisticsDto != null) {
                dbColumnStatisticsDto.setMedian(calculateMedian(jdbcTemplate, tableName, columnName));
                unifyColumnStatistics(dbColumnStatisticsDto);
            }
            return dbColumnStatisticsDto;
        } finally {
            closeConnection(dataSource);
        }
    }

    private Object calculateMedian(JdbcOperations jdbcTemplate, String tableName, String columnName) {
        String countQuery = String.format("SELECT COUNT(%s) FROM %s", columnName, tableName);
        Integer tableCount = jdbcTemplate.queryForObject(countQuery, Integer.class);
        if (tableCount != null && tableCount > 0) {
            int offset;
            int limit;
            if (tableCount % 2 == 0) {
                limit = 2;
                offset = tableCount / 2 - 1;
            } else {
                limit = 1;
                offset = tableCount / 2;
            }
            String limitQuery = String.format("SELECT %s FROM %s ORDER BY %s LIMIT %d OFFSET %d", columnName, tableName, columnName, limit, offset);
            List<Object> columns = jdbcTemplate.queryForList(limitQuery, Object.class);
            if (columns.size() == 1) {
                return columns.get(0);
            } else if (columns.size() == 2) {
                Object firstElement = columns.get(0);
                Object secondElement = columns.get(1);
                if (firstElement instanceof Number && secondElement instanceof Number) {
                    double value1 = ((Number) firstElement).doubleValue();
                    double value2 = ((Number) secondElement).doubleValue();
                    return (value1 + value2) / 2;
                } else {
                    return firstElement;
                }
            }
        }
        return null;
    }

    private void unifyColumnStatistics(DBColumnStatisticsDto dbColumnStatisticsDto) {
        if (dbColumnStatisticsDto.getMedian() == null) {
            return;
        }
        if (dbColumnStatisticsDto.getMedian() instanceof Boolean) {
            if (dbColumnStatisticsDto.getMax() != null) {
                dbColumnStatisticsDto.setMax(dbColumnStatisticsDto.getMax().equals(1));
            }
            if (dbColumnStatisticsDto.getMin() != null) {
                dbColumnStatisticsDto.setMin(dbColumnStatisticsDto.getMin().equals(1));
            }
        }
        if (dbColumnStatisticsDto.getMedian() instanceof String ||
                dbColumnStatisticsDto.getMedian() instanceof Date) {
            dbColumnStatisticsDto.setAvg(null);
        }
    }

    private void closeConnection(HikariDataSource dataSource) {
        if (dataSource != null) {
            dataSource.close();
        }
    }

    private HikariDataSource createDataSource(String connectionName) {
        Optional<DBConnectionDetail> dbConnectionDetailOpt = dbConnectionDetailRepository.findByName(connectionName);
        if (!dbConnectionDetailOpt.isPresent()) {
            throw new DBConnectionDetailNotFoundException("DB connection with name " + connectionName + " not found.");
        }
        DBConnectionDetail dbConnectionDetail = dbConnectionDetailOpt.get();
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName(MYSQL_DRIVER_NAME);
        dataSource.setJdbcUrl("jdbc:mysql://" + dbConnectionDetail.getHostname() + ":" + dbConnectionDetail.getPort() + "/" + dbConnectionDetail.getDatabaseName());
        dataSource.setUsername(dbConnectionDetail.getUsername());
        dataSource.setPassword(passwordService.decryptPassword(dbConnectionDetail.getPassword()));

        return dataSource;
    }
}
