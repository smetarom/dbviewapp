package smetana.dbview.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import smetana.dbview.dto.DBConnectionDetailDto;
import smetana.dbview.entity.DBConnectionDetail;
import smetana.dbview.exception.DBConnectionDetailNotFoundException;
import smetana.dbview.exception.DuplicatedDBConnectionNameException;
import smetana.dbview.repository.DBConnectionDetailRepository;

import java.util.List;
import java.util.Optional;

/**
 * CRUD service for {@link DBConnectionDetail}
 */
@Service
public class DBConnectionService {
    private static Logger logger = LoggerFactory.getLogger(DBConnectionService.class);

    private DBConnectionDetailRepository dbConnectionDetailRepository;
    private PasswordService passwordService;

    public DBConnectionService(DBConnectionDetailRepository dbConnectionDetailRepository, PasswordService passwordService) {
        this.dbConnectionDetailRepository = dbConnectionDetailRepository;
        this.passwordService = passwordService;
    }

    /**
     * Finds all {@link DBConnectionDetail}
     *
     * @return list of {@link DBConnectionDetail}s
     */
    @Transactional
    public Iterable<DBConnectionDetail> findAll() {
        return dbConnectionDetailRepository.findAll();
    }

    /**
     * Persists {@code dbConnectionDetailDto}
     *
     * @param dbConnectionDetailDto create DB connection detail request
     * @return created {@link DBConnectionDetail}
     */
    @Transactional
    public DBConnectionDetail create(DBConnectionDetailDto dbConnectionDetailDto) {
        logger.info("Create DB connection request: " + dbConnectionDetailDto);
        if (dbConnectionDetailRepository.findByName(dbConnectionDetailDto.getName()).isPresent()) {
            throw new DuplicatedDBConnectionNameException("DB name " + dbConnectionDetailDto.getDatabaseName() + " already exists.");
        }
        String encryptPassword = passwordService.encryptPassword(dbConnectionDetailDto.getPassword());
        DBConnectionDetail dbConnectionDetail = new DBConnectionDetail(dbConnectionDetailDto.getName(),
                dbConnectionDetailDto.getHostname(),
                dbConnectionDetailDto.getPort(),
                dbConnectionDetailDto.getDatabaseName(),
                dbConnectionDetailDto.getUsername(),
                encryptPassword);
        dbConnectionDetail = dbConnectionDetailRepository.save(dbConnectionDetail);
        logger.info("Persisted a new DB connection: " + dbConnectionDetail);
        return dbConnectionDetail;
    }

    /**
     * Updates {@code dbConnectionDetailDto}
     *
     * @param id                    ID of updated record
     * @param dbConnectionDetailDto update DB connection detail request
     * @return updated {@link DBConnectionDetail}
     */
    @Transactional
    public DBConnectionDetail update(Long id, DBConnectionDetailDto dbConnectionDetailDto) {
        logger.info("Update DB connection request: " + dbConnectionDetailDto + " for ID " + id);
        Optional<DBConnectionDetail> dbConnectionDetailOpt = dbConnectionDetailRepository.findById(id);
        if (!dbConnectionDetailOpt.isPresent()) {
            throw new DBConnectionDetailNotFoundException("DB connection with ID " + id + " not found.");
        }
        List<DBConnectionDetail> duplicatedNameConnections = dbConnectionDetailRepository.findByNameAndIdNot(dbConnectionDetailDto.getName(), id);
        if (!duplicatedNameConnections.isEmpty()) {
            throw new DuplicatedDBConnectionNameException("DB name " + dbConnectionDetailDto.getDatabaseName() + " already exists.");
        }
        String encryptPassword = passwordService.encryptPassword(dbConnectionDetailDto.getPassword());
        DBConnectionDetail dbConnectionDetail = dbConnectionDetailOpt.get();
        dbConnectionDetail.setName(dbConnectionDetailDto.getName());
        dbConnectionDetail.setHostname(dbConnectionDetailDto.getHostname());
        dbConnectionDetail.setPort(dbConnectionDetailDto.getPort());
        dbConnectionDetail.setDatabaseName(dbConnectionDetailDto.getDatabaseName());
        dbConnectionDetail.setUsername(dbConnectionDetailDto.getUsername());
        dbConnectionDetail.setPassword(encryptPassword);
        dbConnectionDetail = dbConnectionDetailRepository.save(dbConnectionDetail);
        logger.info("Updated connection: " + dbConnectionDetail);
        return dbConnectionDetail;
    }

    /**
     * Deletes {@code dbConnectionDetailDto} by ID
     *
     * @param dbConnectionDetailId ID of deleted record
     */
    @Transactional
    public void delete(Long dbConnectionDetailId) {
        logger.info("Delete DB connection request for id: " + dbConnectionDetailId);
        Optional<DBConnectionDetail> dbConnectionDetailOpt = dbConnectionDetailRepository.findById(dbConnectionDetailId);
        if (!dbConnectionDetailOpt.isPresent()) {
            throw new DBConnectionDetailNotFoundException("DB connection with ID " + dbConnectionDetailId + " not found.");
        }
        dbConnectionDetailRepository.deleteById(dbConnectionDetailId);
    }

    /**
     * Finds {@code dbConnectionDetailDto} by ID
     *
     * @param dbConnectionDetailId ID of selected record
     * @return {@code dbConnectionDetailDto} with ID
     */
    @Transactional
    public DBConnectionDetail findById(Long dbConnectionDetailId) {
        Optional<DBConnectionDetail> dbConnectionDetailOpt = dbConnectionDetailRepository.findById(dbConnectionDetailId);
        if (!dbConnectionDetailOpt.isPresent()) {
            throw new DBConnectionDetailNotFoundException("DB connection with ID " + dbConnectionDetailId + " not found.");
        }
        return dbConnectionDetailOpt.get();
    }
}
