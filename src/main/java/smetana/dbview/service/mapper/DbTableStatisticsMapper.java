package smetana.dbview.service.mapper;

import org.springframework.jdbc.core.RowMapper;
import smetana.dbview.dto.DBTableStatisticsDto;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DbTableStatisticsMapper implements RowMapper<DBTableStatisticsDto> {
    @Override
    public DBTableStatisticsDto mapRow(ResultSet resultSet, int i) throws SQLException {
        int records = resultSet.getInt("RECORDS");
        int attributes = resultSet.getInt("ATTRIBUTES");
        return new DBTableStatisticsDto(records, attributes);
    }
}
