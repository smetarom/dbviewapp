package smetana.dbview.service.mapper;

import org.springframework.jdbc.core.RowMapper;
import smetana.dbview.dto.DBColumnDto;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DbColumnMapper implements RowMapper<DBColumnDto> {
    @Override
    public DBColumnDto mapRow(ResultSet resultSet, int i) throws SQLException {
        String field = resultSet.getString("FIELD");
        String type = resultSet.getString("TYPE");
        boolean nullable = "YES".equalsIgnoreCase(resultSet.getString("NULL"));
        String key = resultSet.getString("KEY");
        String defaultValue = resultSet.getString("DEFAULT");
        String extra = resultSet.getString("EXTRA");
        return new DBColumnDto(field, type, nullable, key, defaultValue, extra);
    }
}
