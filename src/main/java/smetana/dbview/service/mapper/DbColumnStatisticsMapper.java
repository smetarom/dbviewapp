package smetana.dbview.service.mapper;

import org.springframework.jdbc.core.RowMapper;
import smetana.dbview.dto.DBColumnStatisticsDto;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DbColumnStatisticsMapper implements RowMapper<DBColumnStatisticsDto> {
    @Override
    public DBColumnStatisticsDto mapRow(ResultSet resultSet, int i) throws SQLException {
        Object max = resultSet.getObject("MAX");
        Object min = resultSet.getObject("MIN");
        Object avg = resultSet.getObject("AVG");
        return new DBColumnStatisticsDto(min, max, avg, null);
    }
}
