package smetana.dbview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbviewApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbviewApplication.class, args);
    }

}
