package smetana.dbview.repository;

import org.springframework.data.repository.CrudRepository;
import smetana.dbview.entity.DBConnectionDetail;

import java.util.List;
import java.util.Optional;

/**
 * DB connection detail DAO
 */
public interface DBConnectionDetailRepository extends CrudRepository<DBConnectionDetail, Long> {
    Optional<DBConnectionDetail> findByName(String connectionName);

    List<DBConnectionDetail> findByNameAndIdNot(String connectionName, Long id);
}
