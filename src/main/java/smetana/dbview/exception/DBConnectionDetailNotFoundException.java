package smetana.dbview.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * DB connection not found
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class DBConnectionDetailNotFoundException extends RuntimeException {
    public DBConnectionDetailNotFoundException() {
    }

    public DBConnectionDetailNotFoundException(String s) {
        super(s);
    }

    public DBConnectionDetailNotFoundException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DBConnectionDetailNotFoundException(Throwable throwable) {
        super(throwable);
    }
}
