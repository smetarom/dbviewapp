package smetana.dbview.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * DB connection name is not unique
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class DuplicatedDBConnectionNameException extends RuntimeException {
    public DuplicatedDBConnectionNameException() {
    }

    public DuplicatedDBConnectionNameException(String s) {
        super(s);
    }

    public DuplicatedDBConnectionNameException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DuplicatedDBConnectionNameException(Throwable throwable) {
        super(throwable);
    }
}
