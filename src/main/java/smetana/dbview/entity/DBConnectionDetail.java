package smetana.dbview.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

/**
 * Database entity for DB connection detail
 */
@Entity(name = "DB_CONNECTION_DETAIL")
public class DBConnectionDetail {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    /**
     * DB connection name identifier
     */
    private String name;
    /**
     * DB hostname
     */
    private String hostname;
    /**
     * DB port
     */
    private Integer port;
    /**
     * DB name
     */
    private String databaseName;
    /**
     * DB username
     */
    private String username;
    /**
     * DB user password
     */
    private String password;

    public DBConnectionDetail() {

    }

    public DBConnectionDetail(String name, String hostname, Integer port, String databaseName, String username, String password) {
        this.name = name;
        this.hostname = hostname;
        this.port = port;
        this.databaseName = databaseName;
        this.username = username;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DBConnectionDetail that = (DBConnectionDetail) o;
        return name.equals(that.name) &&
                hostname.equals(that.hostname) &&
                port.equals(that.port) &&
                databaseName.equals(that.databaseName) &&
                username.equals(that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, hostname, port, databaseName, username);
    }

    @Override
    public String toString() {
        return "DBConnectionDetail{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", hostname='" + hostname + '\'' +
                ", port=" + port +
                ", databaseName='" + databaseName + '\'' +
                ", username='" + username + '\'' +
                '}';
    }
}
