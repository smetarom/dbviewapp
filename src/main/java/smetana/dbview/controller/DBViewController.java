package smetana.dbview.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import smetana.dbview.dto.DBColumnDto;
import smetana.dbview.dto.DBColumnStatisticsDto;
import smetana.dbview.dto.DBTableStatisticsDto;
import smetana.dbview.service.DBViewService;

import java.util.List;
import java.util.Map;

/**
 * Controller for querying connected DB
 */
@RestController
@RequestMapping("/dbview")
public class DBViewController {
    private DBViewService dbViewService;

    public DBViewController(DBViewService dbViewService) {
        this.dbViewService = dbViewService;
    }

    @GetMapping("/{connectionName}/tables")
    @ApiOperation(value = "Lists all tables in the database")
    public List<String> showTables(@PathVariable @ApiParam(value = "Database connection name from saved connections", required = true) String connectionName) {
        return dbViewService.getTables(connectionName);
    }

    @GetMapping("/{connectionName}/table/{tableName}/columns")
    @ApiOperation(value = "Lists all table columns")
    public List<DBColumnDto> showTableColumns(@PathVariable @ApiParam(value = "Database connection name from saved connections", required = true) String connectionName,
                                              @PathVariable @ApiParam(value = "Requested DB table name", required = true) String tableName) {
        return dbViewService.getTableColumns(connectionName, StringEscapeUtils.escapeSql(tableName));
    }

    @GetMapping("/{connectionName}/table/{tableName}/statistics")
    @ApiOperation(value = "Calculates statistics (number of records and attributes) for a table")
    public DBTableStatisticsDto calculateTableStatistics(@PathVariable @ApiParam(value = "Database connection name from saved connections", required = true) String connectionName,
                                                         @PathVariable @ApiParam(value = "Requested DB table name", required = true) String tableName) {
        return dbViewService.calculateTableStatistics(connectionName, StringEscapeUtils.escapeSql(tableName));
    }

    @GetMapping("/{connectionName}/table/{tableName}/column/{columnName}/statistics")
    @ApiOperation(value = "Calculates statistics (min, max, avg, median) for a table column")
    public DBColumnStatisticsDto calculateColumnStatistics(@PathVariable @ApiParam(value = "Database connection name from saved connections", required = true) String connectionName,
                                                           @PathVariable @ApiParam(value = "Requested DB table name", required = true) String tableName,
                                                           @PathVariable @ApiParam(value = "Name of the column, where statistics are calculated", required = true) String columnName) {
        return dbViewService.calculateColumnStatistics(connectionName, StringEscapeUtils.escapeSql(tableName), StringEscapeUtils.escapeSql(columnName));
    }

    @GetMapping("/{connectionName}/table/{tableName}/data")
    @ApiOperation(value = "Lists all table data")
    public List<Map<String, Object>> showData(@PathVariable @ApiParam(value = "Database connection name from saved connections", required = true) String connectionName,
                                              @PathVariable @ApiParam(value = "Requested DB table name", required = true) String tableName) {
        return dbViewService.getTableData(connectionName, StringEscapeUtils.escapeSql(tableName));
    }
}
