package smetana.dbview.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import smetana.dbview.dto.DBConnectionDetailDto;
import smetana.dbview.entity.DBConnectionDetail;
import smetana.dbview.service.DBConnectionService;

import javax.validation.Valid;

/**
 * Controller with CRUD operations with saved DB connections
 */
@RestController
@RequestMapping("/connection")
public class DBConnectionController {

    private DBConnectionService dbConnectionService;

    public DBConnectionController(DBConnectionService dbConnectionService) {
        this.dbConnectionService = dbConnectionService;
    }

    @GetMapping("/list")
    @ApiOperation(value = "Lists all known database connections")
    public Iterable<DBConnectionDetail> list() {
        return dbConnectionService.findAll();
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Updates an existing database connection by ID")
    public DBConnectionDetail findById(@PathVariable @ApiParam(value = "Database ID of requested DB connection", required = true) Long id) {
        return dbConnectionService.findById(id);
    }

    @PostMapping()
    @ApiOperation(value = "Persists a new database connection into known connection collection")
    public ResponseEntity create(@Valid @RequestBody @ApiParam(value = "Inserted DB connection object", required = true) DBConnectionDetailDto dbConnectionDetailDto) {
        return ResponseEntity.ok(dbConnectionService.create(dbConnectionDetailDto));
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Updates an existing database connection by ID")
    public ResponseEntity<DBConnectionDetail> update(@PathVariable @ApiParam(value = "Database ID of updated DB connection", required = true) Long id,
                                                     @Valid @RequestBody @ApiParam(value = "Updated DB connection object", required = true) DBConnectionDetailDto dbConnectionDetailDto) {
        return ResponseEntity.ok(dbConnectionService.update(id, dbConnectionDetailDto));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Deletes an existing database connection from known connection collection")
    public ResponseEntity delete(@PathVariable @ApiParam(value = "Database ID of deleted DB connection", required = true) Long id) {
        dbConnectionService.delete(id);
        return ResponseEntity.ok().build();
    }
}
