package smetana.dbview.configuration;

import org.jasypt.util.text.BasicTextEncryptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DBViewConfig {
    private static String PASSWORD_SALT = "SALT";

    @Bean
    public BasicTextEncryptor basicTextEncryptor() {
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPasswordCharArray(PASSWORD_SALT.toCharArray());
        return textEncryptor;
    }
}
