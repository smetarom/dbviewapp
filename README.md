## Create MySQL database

CREATE USER 'dbview'@'localhost' IDENTIFIED BY 'password';

CREATE DATABASE dbview;
GRANT ALL PRIVILEGES ON dbview.* TO 'dbview'@'localhost';

CREATE DATABASE dbview_test;
GRANT ALL PRIVILEGES ON dbview_test.* TO 'dbview'@'localhost';

## Swagger REST

http://localhost:8080/swagger-ui.html